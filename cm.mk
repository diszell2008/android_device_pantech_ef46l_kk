## Specify phone tech before including full_phone
$(call inherit-product, vendor/cm/config/gsm.mk)

# Boot animation
TARGET_SCREEN_HEIGHT := 1280
TARGET_SCREEN_WIDTH := 720

# Inherit some common CM stuff.
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

# Enhanced NFC
$(call inherit-product, vendor/cm/config/nfc_enhanced.mk)

# Inherit device configuration
$(call inherit-product, device/pantech/ef46l/ef46l.mk)

## Device identifier. This must come after all inclusions
PRODUCT_DEVICE := ef46l
PRODUCT_NAME := cm_ef46l
PRODUCT_BRAND := pantech
PRODUCT_MODEL := ef46l
PRODUCT_MANUFACTURER := PANTECH

PRODUCT_BUILD_PROP_OVERRIDES += BUILD_UTC_DATE=0

# Enable Torch
PRODUCT_PACKAGES += Torch
